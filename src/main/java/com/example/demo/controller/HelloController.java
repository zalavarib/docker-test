package com.example.demo.controller;

import com.example.demo.document.Request;
import com.example.demo.repository.RequestRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private final RequestRepository repository;

    public HelloController(RequestRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public String hello(@RequestParam(defaultValue = "World") String name){
        return "Everyone says 'Hello, " + name + "!', but no one asks 'How is " + name + "?'.";
    }

    @GetMapping("/save")
    public Object save(@RequestParam(defaultValue = "World") String name){
        return repository.save(new Request(name));
    }

}
