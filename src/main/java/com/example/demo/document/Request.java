package com.example.demo.document;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document("request")
public class Request {

    @Id
    private String id;
    private String param;

    public Request(String param) {
        this.param = param;
    }

}
